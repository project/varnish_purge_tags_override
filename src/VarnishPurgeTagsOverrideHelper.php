<?php

namespace Drupal\varnish_purge_tags_override;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class VarnishPurgeTagsOverrideHelper implements VarnishPurgeTagsOverrideHelperInterface
{
    /**
     * The service container.
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * The default cache backend.
     *
     * @var \Drupal\Core\Cache\CacheBackendInterface
     */
    protected $cache;

    /**
     * The config factory service.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * The entity type manager service.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * The path matcher.
     *
     * @var \Drupal\Core\Path\PathMatcherInterface
     */
    protected $pathMatcher;

    /**
     * The request stack.
     *
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    protected $requestStack;

    /**
     * The current path.
     *
     * @var \Drupal\Core\Path\CurrentPathStack
     */
    protected $currentPath;

    /**
     * An alias manager to find the alias for the current system path.
     *
     * @var \Drupal\path_alias\AliasManagerInterface|null
     */
    protected $aliasManager;


    /**
     * FileRepository constructor.
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     *   The container.
     * @param \Drupal\Core\Config\ConfigFactoryInterface                $configFactory
     *   The config factory service.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface            $entity_type_manager
     *   The entity type manager service.
     * @param \Drupal\Core\Path\PathMatcherInterface                    $path_matcher
     *   The path matcher service.
     * @param \Symfony\Component\HttpFoundation\RequestStack            $request_stack
     *   The request stack.
     * @param \Drupal\Core\Path\CurrentPathStack                        $current_path
     *   The current path.
     */
    public function __construct(
        \Symfony\Component\DependencyInjection\ContainerInterface $container,
        ConfigFactoryInterface $configFactory,
        EntityTypeManagerInterface $entity_type_manager,
        PathMatcherInterface $path_matcher,
        RequestStack $request_stack,
        CurrentPathStack $current_path
    ) {
        $this->container = $container;
        $this->configFactory = $configFactory;
        $this->cache = $container->get("cache.default");
        $this->entityTypeManager = $entity_type_manager;
        $this->pathMatcher = $path_matcher;
        $this->requestStack = $request_stack;
        $this->currentPath = $current_path;
        $this->aliasManager = null;
        /** @phpdoc-ignore-next-line */
        if ($container->has("path_alias.manager")
            && ($alias_manager = $container->get("path_alias.manager"))
            && $alias_manager instanceof \Drupal\path_alias\AliasManagerInterface
        ) {
            $this->aliasManager = $alias_manager;
        }
    }


    /**
     * @inheritDoc
     */
    public function getSettings($bypassCache = false): array
    {
        $configCacheId = sprintf("%s.settings", self::BASE_CACHE_ID);
        $settings = $this->cache->get($configCacheId);
        if ($settings && !$bypassCache) {
            return $settings->data;
        }
        $settings = $this->configFactory->get('varnish_purge_tags_override.settings')->get();
        $this->cache->set($configCacheId, $settings, CacheBackendInterface::CACHE_PERMANENT, ['config:varnish_purge_tags_override.settings', 'config:core.extension']);
        return $settings;
    }

    /**
     * @param  string $key
     * @param  bool  $bypassCache
     * @return int|mixed|string|null
     */
    protected function getSetting($key, $bypassCache = false)
    {
        $settings = $this->getSettings($bypassCache);
        if (!array_key_exists($key, $settings)) {
            return null;
        }

        return $settings[$key];
    }


    /**
     * @inheritDoc
     */
    public function getMaximumHeaderValueSize($bypassCache = false) : int
    {
        return $this->getSetting("maximum_header_value_size", $bypassCache) ?? self::DEFAULT_MAXIMUM_HEADER_VALUE_SIZE;
    }

    /**
     * @inheritDoc
     */
    public function getPages($bypassCache = false) : ?string
    {
        return $this->getSetting("pages", $bypassCache) ?? "";
    }

    public function getContentEntityTypes($bypassCache = false) : array
    {
        $entityTypeList= $this->getSetting("types", $bypassCache) ?? ["node"];
        $entityTypeList = array_values($entityTypeList);

        return array_combine($entityTypeList, $entityTypeList);
    }

    public function isPerBundle($bypassCache = false): bool
    {
        return $this->getSetting("per_bundle", $bypassCache) ?? false;
    }


    public function listContentEntityTypes($bypassCache = false): array
    {
        $cacheId = sprintf("%s.entity_types", self::BASE_CACHE_ID);
        $settings = $this->cache->get($cacheId);
        if ($settings && !$bypassCache) {
            return $settings->data;
        }
        $contentEntityTypes = [];
        $allEntityTypes = $this->entityTypeManager->getDefinitions();
        foreach($allEntityTypes as $entityType) {
            if ($entityType->hasKey("bundle") || ($entityType instanceof ContentEntityTypeInterface)) {
                $contentEntityTypes[$entityType->id()] = sprintf("%s (%s)", $entityType->getLabel(), $entityType->getBaseTable());
            }
        }
        $this->cache->set($cacheId, $contentEntityTypes, CacheBackendInterface::CACHE_PERMANENT, ['config:core.extension', 'entity_bundles']);

        return $contentEntityTypes;
    }

    /**
     * @param string $entity_type
     *
     * @return \Drupal\Core\Entity\EntityInterface []
     */
    protected function getEntities($entity_type) : array
    {
        $entityStorage = $this->entityTypeManager->getStorage($entity_type);
        $query = $entityStorage->getQuery();
        $query->accessCheck(false);
        $ids = array_values($query->execute());
        return $entityStorage->loadMultiple($ids);
    }


    public function getContentEntityTypeBundles($bypassCache = false): array
    {
        $cacheId = sprintf("%s.entity_types_bundles", self::BASE_CACHE_ID);
        $settings = $this->cache->get($cacheId);
        if ($settings && !$bypassCache) {
            return $settings->data;
        }
        $contentEntityTypes = [];
        $allEntityTypes = $this->entityTypeManager->getDefinitions();
        foreach($allEntityTypes as $entityType) {
            if(!$entityType->getBundleEntityType()) {
                continue;
            }
            if ($entityType->hasKey("bundle") || ($entityType instanceof ContentEntityTypeInterface)) {
                $bundles = $this->getEntities($entityType->getBundleEntityType());
                $contentEntityTypes[$entityType->id()] = [];

                foreach($bundles as $bundle) {
                    /** @var \Drupal\Core\Entity\EntityInterface $bundle */
                    $contentEntityTypes[$entityType->id()][$bundle->id()] = sprintf("%s (%s)", $bundle->label(), $bundle->id());
                }
            }
        }
        $this->cache->set($cacheId, $contentEntityTypes, CacheBackendInterface::CACHE_PERMANENT, ['config:core.extension', 'entity_bundles']);

        return $contentEntityTypes;
    }

    /**
     * @inheritDoc
     */
    public function check(Request $request, Response $response) : bool
    {
        /*
        if (!($response instanceof CacheableResponseInterface)
            || $response->headers->hasCacheControlDirective('no-cache')) {
            return FALSE;
        }
        $tags = $response->getCacheableMetadata()->getCacheTags();
        */

        if (!$response->headers->has("Cache-Tags")) {
            return false;
        }
        $cacheTagsHeader = $response->headers->get("Cache-Tags") ?? "";
        $maximum_header_value_size = $this->getMaximumHeaderValueSize();
        if (strlen($cacheTagsHeader) < $maximum_header_value_size) {
            return false;
        }

        // Convert path to lowercase. This allows comparison of the same path
        // with different case. Ex: /Page, /page, /PAGE.
        $pages = mb_strtolower($this->getPages() ?? "");
        if (!$pages) {
            return true;
        }
        // Compare the lowercase path alias (if any) and internal path.
        $path = $this->currentPath->getPath($request);
        // Do not trim a trailing slash if that is the complete path.
        $path = $path === '/' ? $path : rtrim($path, '/');

        if ($this->aliasManager) {
            $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));
            if ($this->pathMatcher->matchPath($path_alias, $pages)) {
                return true;
            }
            if ($path === $path_alias) {
                return false;
            }
        }

        return $this->pathMatcher->matchPath($path, $pages);
    }

    /**
     * @inheritDoc
     */
    public function process(Response $response)
    {
        $cacheTagsHeader = $response->headers->get("Cache-Tags");
        if (!$cacheTagsHeader) {
            return;
        }
        while (str_contains($cacheTagsHeader, "  ")) {
            $cacheTagsHeader = str_replace("  ", " ", $cacheTagsHeader);
        }
        $cacheTagsHeader = trim($cacheTagsHeader);
        $explodedTags = explode(" ", $cacheTagsHeader);
        $keptTags = [];
        $foundEntityTypes = [];

        $bundleSupported = $this->getContentEntityTypes();

        $entityTypeBundles = $this->getContentEntityTypeBundles();
        $perBundle = $this->isPerBundle();
        $entitiesById = [];

        foreach($explodedTags as $tag) {
            $splittedTag = explode(":", $tag);
            $entityType = $splittedTag[0];
            if (!array_key_exists($entityType, $bundleSupported) || (count($splittedTag) != 2) || !is_numeric($splittedTag[1])) {
                $keptTags[$tag] = $tag;
                continue;
            }
            $foundEntityTypes[$entityType] = true;
            if (!$perBundle) {
                continue;
            }
            if (!array_key_exists($entityType, $entitiesById)) {
                $entitiesById[$entityType] = [$splittedTag[1]];
            } else {
                $entitiesById[$entityType][] = $splittedTag[1];
            }
        }

        if (count($foundEntityTypes) === 0) {
            return;
        }
        foreach($foundEntityTypes as $entityType => $value) {

            if (!$perBundle
                || !array_key_exists($entityType, $entityTypeBundles)
            ) {
                $tag = sprintf("%s_list", $entityType);
                $keptTags[$tag] = $tag;
                continue;
            }
            $entityTypeDefinition = $this->entityTypeManager->getDefinition($entityType);
            $bundleKey = $entityTypeDefinition->getKey('bundle');
            $idKey = $entityTypeDefinition->getKey('id');
            if (!$bundleKey || !$idKey) {
                $tag = sprintf("%s_list", $entityType);
                $keptTags[$tag] = $tag;
                continue;
            }

            $entityTypeStorage = $this->entityTypeManager->getStorage($entityType);
            $q = $entityTypeStorage->getAggregateQuery();
                $q->condition($idKey, $entitiesById[$entityType], "IN");
            $q->groupBy($bundleKey);
            $q->accessCheck(false);
            $result = $q->execute();
            foreach($result as $item) {
                /** @var array<string, mixed> $item */
                $entityBundleName = $item[$bundleKey];
                $tag = sprintf("%s_list:%s", $entityType, $entityBundleName);
                $keptTags[$tag] = $tag;
            }
        }
        $keptTags["config:varnish_purge_tags_override.settings"] = "config:varnish_purge_tags_override.settings";
        $cacheTagsHeader = implode(" ", $keptTags);
        $response->headers->set("Cache-Tags", $cacheTagsHeader);
    }

}