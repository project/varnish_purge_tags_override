<?php

namespace Drupal\varnish_purge_tags_override;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface VarnishPurgeTagsOverrideHelperInterface
{
    /**
     * The default allowed extensions.
     */
    public const DEFAULT_MAXIMUM_HEADER_VALUE_SIZE = 8000;

    /**
     * The base cache id to store the configuration.
     */
    public const BASE_CACHE_ID = 'varnish_purge_tags_override.settings';

    /**
     *  get configuration
     *
     * @param  bool $bypassCache set to true to bypass usage of cache
     * @return array<string|int>
     */
    public function getSettings($bypassCache = false): array;

    /**
     * Return maximum_header_value_size from configuration
     *
     * @param  bool $bypassCache set to true to bypass usage of cache
     * @return int
     */
    public function getMaximumHeaderValueSize($bypassCache = false) : int;

    /**
     * @param  bool $bypassCache set to true to bypass usage of cache
     * @return string|null
     */
    public function getPages($bypassCache = false) : ?string;


    /**
     * @param  bool $bypassCache set to true to bypass usage of cache
     * @return bool
     */
    public function isPerBundle($bypassCache = false) : bool;


    /**
     * @param  bool $bypassCache set to true to bypass usage of cache
     * @return array<string>
     */
    public function getContentEntityTypes($bypassCache = false) : array;

    /**
     * @param  false $bypassCache
     * @return array<string,string>
     */
    public function listContentEntityTypes($bypassCache = false) : array;


    /**
     * @param  false $bypassCache
     * @return array<string, array<string, string>>
     */
    public function getContentEntityTypeBundles($bypassCache = false) : array;



    /**
     * @param  Request  $request
     * @param  Response $response
     * @return bool
     */
    public function check(Request $request, Response $response) : bool;


    /**
     * @param  Response $response
     * @return void
     */
    public function process(Response $response);

}